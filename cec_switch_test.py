#!/usr/bin/python3

import cec
import re
import sys
import time

osd_string=sys.argv[1]

def cec_callback_keypress(_event, *args):
    key_code, direction = args
    if direction == 0:
        print('key pressed {}'.format(key_code))


def cec_callback_log(event, level, _log_time, log_message):
    print('LOG: {} {} {}'.format(event, level, log_message))


print('Intialising CEC...')
cec.init()
print('Done')
print("Turning on TV")
tv = cec.Device(0)
tv.power_on()
print()
cec.add_callback(cec_callback_keypress, cec.EVENT_KEYPRESS)
my_address = None
my_number = -1
current_active = -1
print('Scanning devices...')
devices = cec.list_devices()
for device_number in devices:
    device = cec.Device(device_number)
    if device:
        if device.is_active():
            current_active = device_number
        if device.osd_string == osd_string:
            my_address = device.physical_address
            my_number = device_number
        print('number={}, name={}, active={}'.format(device_number, device.osd_string, device.is_active()))

print()
print('Current active device = {}'.format(current_active))
if my_address:
    print('My physical address is {}'.format(my_address))
    int_values = [int(s) for s in re.findall(r'\d', my_address)]
    if len(int_values) != 4:
        print('Hmmm, address for "python-cec" device is not in the expected format')
        sys.exit(1)
    print('Turning on logging...')
    cec.add_callback(cec_callback_log, cec.EVENT_LOG)
    print('Making myself the active source')
    params = bytes([int_values[0] << 4 | int_values[1], int_values[2] << 4 | int_values[3]])
    destination = cec.CECDEVICE_BROADCAST
    opcode = cec.CEC_OPCODE_ACTIVE_SOURCE
    cec.transmit(destination, opcode, params)
    print('Done. Sleeping 5 secs...')
    time.sleep(5)
    print()
    print('So, am I active source?')
    me = cec.Device(my_number)
    print(me.is_active())
else:
    print('Could not find "python-cec" device. This should not happen')
    sys.exit(1)
